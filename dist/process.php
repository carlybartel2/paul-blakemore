<?php
    
    function getResultsFromArray ( $ourArray ) {
      $result = '';
      foreach($ourArray as $ourKey => $ourValue ):
        $result .= $ourValue . "\n\n";
      endforeach;
      return $result;
    }

    $to = "paul.blakemore@me.com"; 
    $from = $_REQUEST['email']; 
    $name = $_REQUEST['name']; 
    $headers = "From: $from"; 
    $subject = "Paul Blakemore Contact Form Submission"; 
    $fields = array(); 
    $fields{"name"} = "Name"; 
    $fields{"email"} = "Email";
    $fields{"tel"} = "Phone"; 
    $fields{"message"} = "Message";
    
    $body = "Contact Submission:\n\n";
    
    foreach($fields as $a => $b):
      if( $a === 'type' ):
        $body .= 'Type:' . getResultsFromArray ( $_REQUEST['type'] );
      else:
        $body .= $b . ': ' . $_REQUEST[$a] . "\n\n";
      endif;
    endforeach;
    $send = mail($to, $subject, $body, $headers);
?>