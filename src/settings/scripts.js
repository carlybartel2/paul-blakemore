/*@codekit-append "../sections/form/form.js"*/

var site;

function Site () {

	var instance, jQueries, internal;
	instance = this;
	internal = {};
	jQueries = {};

	function onReady ( _event ) {
	  window.isReady = true;
	  createFormSection();
	  createCopyrightDate();
	  scrollToTarget();
	  jQuery('.header-menu-button').click(toggleMenu);
    jQuery('.main-nav-link').click(closeMenu);
	}
	
	function createFormSection ( _index, _element ) {
	  var _module;
	  _module = new FormSection();
	  _module.setElementJQuery ( _element );
	  _module.init();
  }
  
  function createCopyrightDate() {
    var date = new Date();
    var year = date.getFullYear();
    document.getElementById("copyright-text").innerHTML = 'Copyright ' + year +  '. All Rights Reserved.';
  }
  
  function scrollToTarget(){
    jQuery('a[href^="#"]').on('click',function (e) {
  	    e.preventDefault();
  	    var target = this.hash;
  	    var $target = $(target);
  
  	    jQuery('html, body').stop().animate({
  	        'scrollTop': $target.offset().top - 43
  	    }, 900, 'swing', function () {
  	        window.location.hash = target;
  	    });
  	});
  } 
  
  function toggleMenu(){
    if (jQuery('.overflow-wrap').hasClass('menu-is-open')){
      jQuery('.overflow-wrap').removeClass('menu-is-open');
      jQuery('.overflow-wrap').addClass('menu-is-closed');
    }
    else {
      jQuery('.overflow-wrap').addClass('menu-is-open');
      jQuery('.overflow-wrap').removeClass('menu-is-closed');
    }
  }
  
  function closeMenu(){
    if (jQuery('.overflow-wrap').hasClass('menu-is-open')){
      jQuery('.overflow-wrap').removeClass('menu-is-open');
      jQuery('.overflow-wrap').addClass('menu-is-closed');
    }
  }
  
  $(window).scroll(function() {    
    var scroll = $(window).scrollTop();
    if (scroll > 100) {
        $(".header").addClass("small");
    } else {
        $(".header").removeClass("small");
    }
});
  
  var swiper = new Swiper('.career-swiper', {
    pagination: '.swiper-pagination',
    grabCursor: true,
    centeredSlides: true,
    loop: true,
    nextButton: '.career-button-next',
    prevButton: '.career-button-prev',
    paginationClickable: true,
    autoHeight: true,
    breakpoints: {
      1920: {
        slidesPerView: 2,
        spaceBetween: 30
      },
      640: {
        slidesPerView: 1,
        spaceBetween: 60
      }
    }
  });
  
  var swiper = new Swiper('.credit-swiper', {
    grabCursor: true,
    centeredSlides: true,
    loop: false,
    nextButton: '.credits-button-next',
    prevButton: '.credits-button-prev',
    autoHeight: true
  });
	
	jQuery( onReady );

}

site = new Site();